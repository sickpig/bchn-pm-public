## Flipstart Bitcoin Cash Node

Bitcoin Cash Node (BCHN) es un proyecto de nodo completo que apunta a proporcionar una implementación de cliente segura para la red BCH, respaldado por una comunidad comprometida de desarrolladores profesionales, testers y personal de soporte.

Nuestro software ya funciona como un reemplazo directo de Bitcoin ABC, y seguirá la cadena más larga durante la actualización de red de Mayo 2020 sin contribuir de manera alguna al riesgo de división de la cadena.

Nuestra visión para BCH es la de una red que sea robusta mediante la competencia y la cooperación, que sea ejecutada en una diversa gama de programas de cliente, que cumpla con los niveles profesionales de desarrollo, mantenimiento y soporte. Ayudaremos a crear un entorno en el que las decisiones estén respaldadas por la investigación y la evidencia, así como que la toma de decisiones involucre las partes interesadas y experiencias de todo el ecosistema.

El proyecto Bitcoin Cash Node cuenta con una política de puertas abiertas, dando la bienvenida a todos los niveles de experiencia desde todos los rincones de Bitcoin Cash. Discutimos los asuntos abiertamente, invitamos a las partes interesadas a participar y nos mantenemos bajo altos estándares de responsabilidad.

En esta propuesta de financiación, esperamos obtener los recursos necesarios para atender algunas de las necesidades inmediatas de los mineros, empresas y usuarios de Bitcoin Cash, proporcionando una sólida investigación e implementación para ellos. También tenemos la intención de establecer la estructura necesaria para asegurarnos de estar debidamente equipados para mantenernos en la próxima etapa de desafíos. 
